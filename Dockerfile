FROM openjdk:8-jdk

LABEL MAINTAINER "RAKESH"

ARG IMAGE_TAG

ENV ENTRY_ARG=$IMAGE_TAG

RUN useradd -ms /bin/bash user
RUN chown -Rf user:user  "/opt" 
USER user

WORKDIR "/opt/"

COPY [ "/target/assignment-$IMAGE_TAG.jar","entrypoint.sh","/opt/"]

ENTRYPOINT ["/opt/entrypoint.sh"]

